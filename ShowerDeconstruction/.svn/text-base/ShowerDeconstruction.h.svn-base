// this file is -*- C++ -*-
#ifndef SHOWERDECONSTRUCTION_H
#define SHOWERDECONSTRUCTION_H

/*
 *  This tool implements the Shower Deconstruction method, described in the following two papers.
 *  If you use this, please cite those papers in your publication.
 *  D. E. Soper and M. Spannowsky, ``Finding physics signals with shower deconstruction,''  Phys. Rev. D 84 (2011) 074002 [arXiv:1102.3480 [hep-ph]]
 *  D. E. Soper and M. Spannowsky, ``Finding top quarks with shower deconstruction,'' Phys. Rev. D 87 (2013) 5,  054012 [arXiv:1211.3140 [hep-ph]]
 *
 * In case of questions, please contact Danilo.Enoque.Ferreira.De.Lima@cern.ch or Michael.Spannowsky@cern.ch.
 *
 */

#include <iostream>
#include <fstream>
#include <string>
#include <cmath>

#include "fastjet/PseudoJet.hh"
#include "fastjet/ClusterSequence.hh"

#include "ShowerDeconstruction/AnalysisParameters.h"
#include "ShowerDeconstruction/Exception.h"

#include "ShowerDeconstruction/TopGluonModel.h"
#include "ShowerDeconstruction/BackgroundModel.h"
#include "ShowerDeconstruction/Model.h"
#include "ShowerDeconstruction/ISRModel.h"
#include "ShowerDeconstruction/Deconstruct.h"

#include "AsgTools/AsgTool.h"
#include "AsgTools/ToolHandle.h"

#include "xAODJet/JetContainer.h"

#include "JetInterface/IJetFromPseudojet.h"
#include "JetInterface/IJetModifier.h"
#include "JetCalibTools/IJetCalibrationTool.h"

class ShowerDeconstruction : public asg::AsgTool,
                             virtual public IJetModifier { 
    ASG_TOOL_CLASS(ShowerDeconstruction, IJetModifier)
    public:

    /// Default constructor
    ShowerDeconstruction(const std::string& name);
    /// Default destructor
    ~ShowerDeconstruction();

    StatusCode initialize();

    /// Implements IJetModifier
    /// Builds and records tagged jets from the input jet container.
    int modify(xAOD::JetContainer& jets) const;
    int tag(xAOD::Jet *jet) const;

    // tag a single jet composed of the given subjets
    // for each subjet, the entry in the vector btag for the same index can be 1, 0 or -1
    // if the btag item is 1, it means the subjet is btagged
    // if the btag item is -1, it means the subjet failed btagging
    // if the btag item is 0, it means that the subjet cannot be btagged (too low or too high pt, for example)
    double tagJetFromSubjets(std::vector<fastjet::PseudoJet> &subjets, std::vector<int> &btag) const;
   
  private:
    std::string m_name;

    std::string m_mode;
    float m_R;
    float m_maxNjets;
    float m_topMass;
    float m_topMassWindow;
    float m_wMass;
    float m_wMassWindow;
    float m_hMass;
    float m_hMassWindow;
    float m_lambda_mu_ext;
    float m_lambda_theta_ext;
    float m_fractionISR;
    int m_noISR;
    float m_tagprob;
    float m_fakeprob;
    int m_useBtag;

    AnalysisParameters *m_param;
    Deconstruction::Deconstruct *m_mainTool;
    Deconstruction::ISRModel *m_isr;
    Deconstruction::Model *m_signal;
    Deconstruction::Model *m_background;

}; // class ShowerDeconstruction

#endif
