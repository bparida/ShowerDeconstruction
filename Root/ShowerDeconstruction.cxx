#include "ShowerDeconstruction/ShowerDeconstruction.h"

#include "xAODJet/JetAuxContainer.h"
#include "JetEDM/IConstituentUserInfo.h"
#include <fastjet/PseudoJetStructureBase.hh>
#include <fastjet/SharedPtr.hh>

#include "fastjet/JetDefinition.hh"
#include "fastjet/ClusterSequenceArea.hh"
#include "JetEDM/JetConstituentFiller.h"

#include "ShowerDeconstruction/TopGluonModel.h"
#include "ShowerDeconstruction/WDecayModel.h"
#include "ShowerDeconstruction/BackgroundModel.h"
#include "ShowerDeconstruction/HBBModel.h"

#include "xAODBase/IParticle.h"

#include <vector>
#include <cmath>
#include <string>

ShowerDeconstruction::ShowerDeconstruction(const std::string& name) : 
  asg::AsgTool(name),
  m_mode("t/g"),                            // options in the format "a/b", where a or b are one of t, q, g, w for top, quark, gluon or W tagger
  m_R(1.0),                                   // large-R jet R parameter
  m_maxNjets(9),                              // maximum number of subjets to use (to speed it up)
  m_topMass(172.00),                          // top mass [GeV]
  m_topMassWindow(50.00),                     // top mass window [GeV]
  m_wMass(80.403),                            // W mass [GeV]
  m_wMassWindow(20.00),                       // W mass window [GeV]
  m_hMass(125.00),                            // Higgs mass [GeV]
  m_hMassWindow(20.00),                       // Higgs mass window [GeV]
  m_lambda_mu_ext(1.00),                      // lambda mu param.
  m_lambda_theta_ext(1.00),                   // lambda theta param.
  m_fractionISR(0.50),                        // fraction ISR of hard interaction pt
  m_noISR(0),                                 // do not include ISR model?
  m_tagprob(0.70),                            // b-tagging probability
  m_fakeprob(0.01),                           // c/l-tagging probability
  m_useBtag(0)                               // use b-tagging status of subjets?
{

  declareProperty("mode", m_mode);
  declareProperty("R", m_R);
  declareProperty("maxNjets", m_maxNjets);
  declareProperty("topMass", m_topMass);
  declareProperty("topMassWindow", m_topMassWindow);
  declareProperty("wMass", m_wMass);
  declareProperty("wMassWindow", m_wMassWindow);
  declareProperty("hMass", m_hMass);
  declareProperty("hMassWindow", m_hMassWindow);
  declareProperty("lambda_mu_ext", m_lambda_mu_ext);
  declareProperty("lambda_theta_ext", m_lambda_theta_ext);
  declareProperty("fractionISR", m_fractionISR);
  declareProperty("tagprob", m_tagprob);
  declareProperty("fakeprob", m_fakeprob);
  declareProperty("useBtag", m_useBtag);

  m_param = 0;
  m_signal = 0;
  m_background = 0;
  m_isr = 0;
  m_mainTool = 0;
} 

ShowerDeconstruction::~ShowerDeconstruction() {
  if (m_param) {
    delete m_param;
    m_param = 0;
  }

  if (m_signal) {
    delete m_signal;
  }
  if (m_background) {
    delete m_background;
  }
  if (m_isr) {
    delete m_isr;
  }
  if (m_mainTool) {
    delete m_mainTool;
  }
}

StatusCode ShowerDeconstruction::initialize(){

  m_param = new AnalysisParameters;
  (*m_param)["R"] = m_R;
  (*m_param)["lambda_mu_ext"] = m_lambda_mu_ext;
  (*m_param)["lambda_thetha_ext"] = m_lambda_theta_ext;
  (*m_param)["fractionISR_of_hardInt_PT"] = m_fractionISR;
  (*m_param)["noISR"] = m_noISR;
  (*m_param)["topmass"] = m_topMass;
  (*m_param)["topwindow"] = m_topMassWindow;
  (*m_param)["wmass"] = m_wMass;
  (*m_param)["wwindow"] = m_wMassWindow;
  (*m_param)["Higgsmass"] = m_hMass;
  (*m_param)["delta_Higgsmass"] = m_hMassWindow;
  (*m_param)["br_wqq"] = 0.6666666666;
  (*m_param)["useBtag"] = m_useBtag;
  (*m_param)["tagprob"] = m_tagprob;
  (*m_param)["fakeprob"] = m_fakeprob;

  m_isr = new Deconstruction::ISRModel(*m_param);
  if (m_mode.size() != 3) {
    // m_mode needs to be in the format a/b
    return StatusCode::FAILURE;
  }
  switch (m_mode[0]) {
    case 't':
      m_signal = new Deconstruction::TopGluonModel(*m_param);
      break;
    case 'g':
      m_signal = new Deconstruction::BackgroundModel(*m_param, Deconstruction::Flavour::g);
      break;
    case 'q':
      m_signal = new Deconstruction::BackgroundModel(*m_param, Deconstruction::Flavour::q);
      break;
    case 'w':
      m_signal = new Deconstruction::WDecayModel(*m_param);
      break;
    case 'h':
      m_signal = new Deconstruction::HBBModel(*m_param);
      break;
    default:
      return StatusCode::FAILURE;
      break;
  }
  switch (m_mode[2]) {
    case 't':
      m_background = new Deconstruction::TopGluonModel(*m_param);
      break;
    case 'g':
      m_background = new Deconstruction::BackgroundModel(*m_param, Deconstruction::Flavour::g);
      break;
    case 'q':
      m_background = new Deconstruction::BackgroundModel(*m_param, Deconstruction::Flavour::q);
      break;
    case 'w':
      m_background = new Deconstruction::WDecayModel(*m_param);
      break;
    case 'h':
      m_background = new Deconstruction::HBBModel(*m_param);
      break;
    default:
      return StatusCode::FAILURE;
      break;
  }
  m_mainTool = new Deconstruction::Deconstruct(*m_param, *m_signal, *m_background, *m_isr);

  return StatusCode::SUCCESS;
}


int ShowerDeconstruction::modify(xAOD::JetContainer& jets) const {
  int res = 0;
  for (xAOD::Jet *jet : jets) {
    res += tag(jet);
  }
  return res; // error code
}

double ShowerDeconstruction::tagJetFromSubjets(std::vector<fastjet::PseudoJet> &subjets, std::vector<int> &btagged) const {
  
  // copy and set b-tag status
  std::vector<fastjet::PseudoJet> microjets;
  for (int i = 0; i < subjets.size(); ++i) {
    microjets.push_back(subjets[i]);
    microjets[i].set_user_index(btagged[i]);
  }

  if(microjets.size() > m_maxNjets) {
    microjets.erase(microjets.begin() + (int) m_maxNjets, microjets.begin() + microjets.size());
  }

  for (unsigned int k = 0; k < microjets.size(); ++k) {
    microjets[k] *= 1e-3;
    // apply an 100 MeV shift, to avoid singularities in SD for some cases
    // the calibration usually solves this problem
    double shift = 0;
    if (microjets[k].m() < 0.1) {
      shift = 0.1;
    }
    microjets[k].reset_momentum(microjets[k].px(), microjets[k].py(), microjets[k].pz(), microjets[k].e()+shift);
  }

  double chi = -100;
  double wSignal = -100, wBackground = -100;
  chi = m_mainTool->deconstruct(microjets, wSignal, wBackground);
  double lchi = -100;
  if (chi > 0) lchi = std::log(chi);

  return lchi;
}

int ShowerDeconstruction::tag(xAOD::Jet *jet) const {
  fastjet::JetDefinition microjet_def(fastjet::cambridge_algorithm, 0.2);

  xAOD::JetConstituentVector vec = jet->getConstituents();
  if (!vec.isValid()) { // this will happen as some derivations only keep the jet's constituents for jets passing some pt cuts!
    // this does not mean something is wrong, but uncomment the line below to debug it
    //std::cout << "Some ElementLinks are not valid in this jet. Check if all the constituents were stored. (pt, eta, phi, m = " << jet->pt() << ", " << jet->eta() << ", " << jet->phi() << ", m = " << jet->m() << std::endl;
    return -1;
  }
  xAOD::JetConstituentVector::iterator it = vec.begin();
  xAOD::JetConstituentVector::iterator itE = vec.end();
  std::vector<fastjet::PseudoJet> constituents;
  for (; it != itE; ++it) {
    fastjet::PseudoJet p(0,0,0,0);
    const xAOD::JetConstituent *jc = *it;
    if (!jc) continue;
    float pt = jc->pt();
    float y = jc->rapidity();
    float phi = jc->phi();
    float m = jc->m();
    if (y != y) {
      continue; // null vectors cause NaNs
    } else {
      p.reset_PtYPhiM(pt, y, phi, m);
      constituents.push_back(p);
    }
  }
  
  fastjet::ClusterSequence microjet_cs(constituents, microjet_def);
  float pt_cut = 20e3; // 20 GeV cut
  std::vector<fastjet::PseudoJet> microjets = fastjet::sorted_by_pt(microjet_cs.inclusive_jets(pt_cut));
  // they need to be calibrated here (TODO)

  std::vector<int> btag(microjets.size(), 0); // ignore b-tagging status
  jet->auxdata<float>(std::string("ShowerDeconstruction_log_chi_")+m_mode) = (float) tagJetFromSubjets(microjets, btag);
  return 0;
}


